const chartColors = {
  green: '#59B858',
  red: '#DB534B'
};

export default chartColors;
