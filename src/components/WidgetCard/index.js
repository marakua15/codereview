import React from 'react';
import { withStyles } from "@material-ui/core";
import { chartColors, fuseDark } from 'theme/@fuse/fuse-colors'
import Button from '@material-ui/core/Button';
import ClearIcon from '@material-ui/icons/Clear';

const styles = () => ({
  card: {
    width: "100%",
    backgroundColor: fuseDark.A100,
  },
  green: {
    backgroundColor: chartColors.green,
  },
  red: {
    backgroundColor: chartColors.red,
  },
  header: {
    position: "relative",
    backgroundColor: "#32353A",
    color: "#fff",
    padding: "20px 20px",
    fontSize: "16px",
    '& span': {
      display: 'block',
      maxWidth: '100%',
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
    },
  },
  removeBtn: {
    position: 'absolute',
    right: '5px',
    top: '5px',
    width: '18px',
    height: '18px',
    minHeight: '18px',
  },
  removeBtnIcon: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    width: '12px',
    height: '12px',
    transform: 'translate(-50%, -50%)'
  },
});

const WidgetCard = ({ title, theme, onRemove, children, classes }) => (
  <div className={`${classes.card} ${classes[theme]}`}>
    <div className={classes.chartWrap}>
      <div className={classes.header}>
        <span>{title}</span>

        <Button
          variant='fab' mini color='default' aria-label='Add' className={classes.removeBtn}
          onClick={() => onRemove}
        >
          <ClearIcon fontSize='small' className={classes.removeBtnIcon}/>
        </Button>
      </div>

      <div className={classes.cardContent}>
        {children}
      </div>
    </div>
  </div>
);

export default withStyles(styles)(WidgetCard);
