import { chartColors } from 'theme/@fuse/fuse-colors'

const setChartConfig = (data, isPositive) => ({
    "type": "serial",
    "theme": "light",
    "marginRight": 0,
    "marginLeft": 0,
    "autoMarginOffset": 0,
    "dataDateFormat": "YYYY-MM-DD",
    "balloon": {
      "borderThickness": 1,
      "shadowAlpha": 0
    },
    "graphs": [
      {
        "balloonText": "",
        "useLineColorForBulletBorder": true,
        "descriptionField": "week",
        "labelPosition": "right",
        "labelText": "",
        "legendValueText": "",
        "title": "",
        "fillAlphas": 0,
        "valueField": "value",
        "valueAxis": "",
        "connect": false,

        "lineThickness": 3,
        "lineColor": "#fff",

        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": isPositive > 0 ? chartColors.green : chartColors.red,
        "bulletSize": 7,
        "bulletBorderThickness": 2,
      }],
    "chartCursor": {
      "enabled": false,
    },
    "categoryField": "date",
    "categoryAxis": {
      "gridPosition": "start",
      "axisAlpha": 0,
      "gridAlpha": 0,
      "minHorizontalGap": 0,
      "visible": false
    },
    "valueAxes": [
      {
        "position": "left",
        "axisAlpha": 0,
        "autoWrap": false,
        "gridAlpha": 0,
        "visible": false,
        "tickLength": 2,
      }],
    "export": {
      "enabled": false
    },
    "dataProvider": data
  });

export default setChartConfig;

