import React, { Fragment } from 'react';
import { withStyles } from "@material-ui/core";
import WidgetCard from "components/WidgetCard";

import './style.css';

const styles = () => ({});

const Chart = ({ data, title, changeRate, changeValue, onRemove }) => {
  const isPositive = changeValue >= 0;

  return (
    <WidgetCard
      title={title}
      onRemove={onRemove}
      theme={isPositive ? 'green' : 'red'}
    >
      <Fragment>
        {data}
        {changeRate}
      </Fragment>
    </WidgetCard>
  )
};

export default withStyles(styles)(Chart);
