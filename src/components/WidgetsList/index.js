import React from "react";
import BusinessSalesPerfomance from 'components/widgets/BusinessSalesPerfomance';
import { withStyles } from "@material-ui/core";
import {
  DraggableItem,
  DraggableContainer,
} from '@wuweiweiwu/react-shopify-draggable';

import './style.css';

const widgestToShow = [
  {
    id: '1',
    Item: BusinessSalesPerfomance
  },
  {
    id: '12',
    Item: BusinessSalesPerfomance
  },
  {
    id: '11',
    Item: BusinessSalesPerfomance
  },
  {
    id: '133',
    Item: BusinessSalesPerfomance
  },
];

const styles = theme => ({
  grid: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    '& > *': {
      minWidth: 0,
    }
  },
  col: {
    flex: '0 0 33.333%',
    paddingLeft: 2 * theme.spacing.unit,
    paddingRight: 2 * theme.spacing.unit,
    marginBottom: 4 * theme.spacing.unit,
  },
});

const List = ({ classes }) => (
  <DraggableContainer
    as='div'
    type='swappable'
    className={`BlockGenerator ${classes.grid}`}
    swapAnimation={{ duration: 150, easingFunction: 'ease-in-out' }}
  >
    {
      widgestToShow.map(({ id, Item }) => (
        <DraggableItem
          as='div'
          className={`drag-block ${classes.col}`}
          key={id}
        >
          <div className='drag-block-content'>
            <Item/>
          </div>
        </DraggableItem>
      ))
    }
  </DraggableContainer>
);

export default withStyles(styles)(List);

