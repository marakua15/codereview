import React, { Fragment } from 'react';
import AmCharts from '@amcharts/amcharts3-react';
import { withStyles } from "@material-ui/core";
import WidgetCard from "components/WidgetCard";
import setChartConfig from './config';

import './style.css';

const styles = () => ({
  chart: {
    position: 'relative',
    height: "120px",
    width: "100%",
    // "&:after": {
    //   'content': '""',
    //   'display': 'block',
    //   'paddingTop': '50%',
    // }
  },
  chartWrap: {
    width: "100%",
  },
  change: {
    width: "100%",
    padding: "10px 15px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    '& > *': {
      minWidth: 0,
    }
  },
  changeRate: {
    fontSize: "32px",
    color: "#fff",
    fontWeight: '700'
  },
  changeValue: {
    fontSize: "36px",
    color: "#fff",
    fontWeight: '300'
  },
});

const Chart = ({ data, title, changeRate, changeValue, onRemove, classes }) => {
  const isPositive = changeValue >= 0;

  return (
    <WidgetCard
      title={title}
      onRemove={onRemove}
      theme={isPositive ? 'green' : 'red'}
    >
      <Fragment>
        <AmCharts.React
          className={classes.chart}
          options={setChartConfig(data, isPositive)}
        />

        <div className={classes.change}>
          <div className={classes.changeRate}>
            {changeRate > 0 ? '+' : ''}
            {changeRate < 0 ? '-' : ''}
            {Math.round(changeRate * 100) / 100}%
          </div>

          <div className={classes.changeValue}>
            {
              changeValue > 0
                ? <span>+${changeValue}M</span>
                : <span>-${changeValue * -1}M</span>
            }
          </div>
        </div>
      </Fragment>
    </WidgetCard>
  )
};

export default withStyles(styles)(Chart);
