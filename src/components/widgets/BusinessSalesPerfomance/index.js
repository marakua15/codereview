import React from "react";
import Chart from "components/LinearWidgetChart";

const data = [
  {
    'week': 1,
    'value': 1000000,
  },
  {
    'week': 1,
    'value': 1200000,
  },
  {
    'week': 1,
    'value': 1400000,
  },
  {
    'week': 1,
    'value': 2200000,
  },
  {
    'week': 1,
    'value': 1900000,
  },
  {
    'week': 1,
    'value': 2000000,
  },
  {
    'week': 1,
    'value': 2100000,
  },
  {
    'week': 1,
    'value': 1800000,
  },
  {
    'week': 1,
    'value': 1500000,
  }
];

const Widget = () => (
  <div>
    <Chart
      data={data}
      title='Business Sales Performance'
      changeRate={.5}
      changeValue={100}
      onRemove={() => {}}
    />
  </div>
);

export default Widget;
