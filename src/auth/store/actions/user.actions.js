import history from 'history.js';
import { setDefaultSettings } from 'store/actions/fuse';
import { FuseDefaultSettings } from 'theme/@fuse';
import _ from 'lodash';

export const SET_USER_DATA = '[USER] SET DATA';
export const REMOVE_USER_DATA = '[USER] REMOVE DATA';
export const USER_LOGGED_OUT = '[USER] LOGGED OUT';


/**
 * Set User Data
 */
export function setUserData(user) {
  return (dispatch) => {

    /*
    Set User Settings
     */
    dispatch(setDefaultSettings(user.data.settings));

    /*
    Set User Data
     */
    dispatch({
      type: SET_USER_DATA,
      payload: user
    })
  }
}

/**
 * Update User Data
 */
function updateUserData(user) {
  if(user.role === 'guest') {
    return user.role;
  }

  return 0;
}

/**
 * Update User Settings
 */
export function updateUserSettings(settings) {
  return (dispatch, getState) => {
    const oldUser = getState().auth.user;
    const user = _.merge({}, oldUser, { data: { settings } });

    updateUserData(user);

    return dispatch(setUserData(user));
  }
}

/**
 * Update User Shortcuts
 */
export function updateUserShortcuts(shortcuts) {
  return (dispatch, getState) => {
    const { user } = getState().auth;
    const newUser = {
      ...user,
      data: {
        ...user.data,
        shortcuts
      }
    };

    updateUserData(newUser);

    return dispatch(setUserData(newUser));
  }
}

/**
 * Remove User Data
 */
export function removeUserData() {
  return {
    type: REMOVE_USER_DATA
  }
}

/**
 * Logout
 */
export function logoutUser() {
  history.push({
    pathname: '/'
  });

  return (dispatch) => {

    dispatch(setDefaultSettings(FuseDefaultSettings));

    dispatch({
      type: USER_LOGGED_OUT
    })
  }
}
