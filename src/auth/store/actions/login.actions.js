import axios from 'axios/index';
import { setUserData } from 'auth/store/actions/user.actions';

export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

export function submitLogin({ email, password }) {
    const request = axios.get('/api/auth', {
        data: {
            email,
            password
        }
    });
    return (dispatch) =>
        request.then((response) => {
            if(!response.data.error) {
                dispatch(setUserData(response.data));
                return dispatch({
                    type: LOGIN_SUCCESS
                });
            }
            
                return dispatch({
                    type: LOGIN_ERROR,
                    payload: response.data.error
                });
            
        });
}

