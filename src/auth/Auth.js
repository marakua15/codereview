import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from 'store/actions';

const Auth = ({ children }) => (
  <Fragment>
    {children}
  </Fragment>
)

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
      showMessage: Actions.showMessage,
      hideMessage: Actions.hideMessage
    },
    dispatch);
}

export default connect(null, mapDispatchToProps)(Auth);
