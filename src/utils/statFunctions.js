export const getChangeRate = (arr) => arr[arr.length - 1] / arr[0];
export const getChangeValue = (arr) => arr[arr.length - 1] - arr[0];
