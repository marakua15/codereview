import Provider from "react-redux/es/components/Provider";
import { Router } from "react-router-dom";
import React from "react";
import { FuseLayout, FuseTheme, FuseAuthorization } from 'theme/@fuse';
import JssProvider from 'react-jss/lib/JssProvider';
import { create } from "jss";
import { createGenerateClassName, jssPreset } from "@material-ui/core";
import jssExtend from "jss-extend";
import store from "../store";
import { Auth } from "../auth";
import history from "../history";
import MainToolbar from "../main/MainToolbar";
import MainNavbarHeader from "../main/MainNavbarHeader";
import MainNavbarContent from "../main/MainNavbarContent";
import MainFooter from "../main/MainFooter";
import QuickPanel from "../main/quickPanel/QuickPanel";
import routes from './routes';
import '../react-table-defaults';
import '../styles/index.css';

const jss = create({
  ...jssPreset(),
  plugins: [...jssPreset().plugins, jssExtend()]
});

jss.options.insertionPoint = document.getElementById('jss-insertion-point');
const generateClassName = createGenerateClassName();

const routerConfig = () => (
  <JssProvider jss={jss} generateClassName={generateClassName}>
    <Provider store={store}>
      <Auth>
        <Router history={history}>
          <FuseAuthorization routes={routes}>
            <FuseTheme>
              <FuseLayout
                routes={routes}
                toolbar={
                  <MainToolbar/>
                }
                navbarHeader={
                  <MainNavbarHeader/>
                }
                navbarContent={
                  <MainNavbarContent/>
                }
                footer={
                  <MainFooter/>
                }
                rightSidePanel={
                  <React.Fragment>
                    <QuickPanel/>
                  </React.Fragment>
                }
              />
            </FuseTheme>
          </FuseAuthorization>
        </Router>
      </Auth>
    </Provider>
  </JssProvider>
)

export default routerConfig;
