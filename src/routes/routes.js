import React from 'react';
import { Redirect } from 'react-router-dom';

import Dashboard from './Dashboard';

const routes = [
  {
    exact: true,
    path: '/',
    component: () => <Redirect to='/dashboard'/>
  },
  {
    path: '/dashboard',
    component: () => <Dashboard />
  }
];

export default routes;
