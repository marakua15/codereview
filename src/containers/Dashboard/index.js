import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import WidgetsList from 'components/WidgetsList';

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#496474',
    paddingTop: 3 * theme.spacing.unit,
  },
  container: {
    width: '1200px',
    maxWidth: '100%',
    margin: '0 auto',
  },
  col: {
    paddingLeft: 2 * theme.spacing.unit,
    paddingRight: 2 * theme.spacing.unit,
    marginBottom: 2 * theme.spacing.unit,
  },
  paper: {
    padding: theme.spacing.unit,
  },
});

const Dashboard = ({ classes }) => (
  <div className={classes.root}>
    <Grid container>
      <WidgetsList/>
    </Grid>
  </div>
);

export default withStyles(styles)(Dashboard);
