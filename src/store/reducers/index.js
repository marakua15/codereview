import { combineReducers } from 'redux';
import auth from 'auth/store/reducers';
import quickPanel from 'main/quickPanel/store/reducers';
import fuse from './fuse';

const rootReducer = combineReducers({
    auth,
    fuse,
    quickPanel
});

export default rootReducer;
