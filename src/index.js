import 'babel-polyfill'
import 'typeface-muli';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import routerConfig from './routes';

ReactDOM.render(
    routerConfig(),
    document.getElementById('root')
);
registerServiceWorker();
