import { applyMiddleware, compose, createStore } from "redux";
import reducers from "store/reducers";
import thunk from "redux-thunk";

const composeEnhancers =
  typeof window === "object" &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? // eslint-disable-line no-underscore-dangle
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose; // eslint-disable-line no-underscore-dangle

const enhancer = composeEnhancers(
  applyMiddleware(thunk)
);

const store = createStore(reducers, enhancer);

export default store;
